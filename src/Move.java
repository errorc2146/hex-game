public class Move implements MoveInterface
{
    private int x,y;
    private boolean concede;
    
    public Move()
    {
        this.x = 0;
        this.y = 0;
        this.concede = false;
    }
    
    public boolean setPosition(int x, int y) throws InvalidPositionException
    {
        if (x < 0 || y < 0) throw new InvalidPositionException();
        this.x = x;
        this.y = y;
        return true;
    }
    
    public boolean hasConceded()
    {
        return concede;
    }
    
    public int getXPosition()
    {
        return x;
    }
    
    public int getYPosition()
    {
        return y;
    }
    
    public boolean setConceded()
    {
        concede = true;
        return true;
    }
    
    public String toString()
    {
        return "(" + x + "," + y + ")";
    }
}

