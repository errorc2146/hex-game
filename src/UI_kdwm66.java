import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class UI_kdwm66 extends JFrame implements ComponentListener
{
    private static UI_kdwm66 instance;
    
    private int SWIDTH;
    private Piece[][] board;
    private RenderPanel cPanel;
    private int currentX, currentY;
    private boolean quit;
    private boolean userDone;
    private Piece colour;

    protected UI_kdwm66()
    {
        super();
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        SWIDTH = 760;

        currentX = 0;
        currentY = 0;

        this.board = null;
        this.colour = colour;
        
        setTitle("Hex");
        setSize(SWIDTH, SWIDTH);

        setFocusable(true);
        addKeyListener(new cKeyListener());
        
        setBackground(Color.WHITE);
        
        userDone = false;
        
        quit = false;
        
        setUndecorated(true);
        getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
        
        addWindowListener(new WindowAdapter() 
        { 
            public void windowClosing(WindowEvent e)
            { 
                concede();
                if (quit) dispose();
            }
        });
        
        addComponentListener(this);
    }
    
    public static UI_kdwm66 getInstance()
    {
        if (instance == null)
        {
            instance = new UI_kdwm66();
        }
        return instance;
    }
    
    public void componentResized(ComponentEvent e) 
    {
        Rectangle newSize = e.getComponent().getBounds();
        SWIDTH = Math.max(newSize.width, newSize.height);
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int limit = Math.min((int)screenSize.getWidth(), (int)screenSize.getHeight());
        
        SWIDTH = Math.min(SWIDTH, limit);
        
        setSize(SWIDTH, SWIDTH);
        if (cPanel != null) cPanel.calculateHex();
    }
    public void componentHidden(ComponentEvent e){}
    public void componentMoved(ComponentEvent e){}
    public void componentShown(ComponentEvent e){}
    
    private void concede()
    {
        String message = "Are you sure you want to give up?";
        int response = JOptionPane.showConfirmDialog(null, message, "Had enough?", JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            userDone = true;
            quit = true;
        }
    }
    
    public void setColour(Piece colour)
    {
        this.colour = colour;
    }

    public void setBoard(Piece[][] board)
    {
        this.board = board;
        userDone = false;
        
        if (cPanel == null)
        {
            Container contentPane = getContentPane();
            cPanel = new RenderPanel();
            contentPane.add(cPanel);
            setVisible(true);
        }
        
        repaint();
    }

    private class cKeyListener implements KeyListener
    {
        public void keyTyped(KeyEvent e){}
        public void keyReleased(KeyEvent e){}

        public void keyPressed(KeyEvent e)
        {
            if (board == null) return;

            if (e.getKeyCode() == KeyEvent.VK_RIGHT && currentX < board.length - 1) currentX++;
            else if (e.getKeyCode() == KeyEvent.VK_LEFT && currentX > 0) currentX--;
            else if (e.getKeyCode() == KeyEvent.VK_DOWN && currentY < board[0].length - 1) currentY++;
            else if (e.getKeyCode() == KeyEvent.VK_UP && currentY > 0) currentY--;
            else if (e.getKeyCode() == KeyEvent.VK_ENTER) 
            {
                userDone = true;
            }
            else if (e.getKeyCode() == KeyEvent.VK_Q)
            {
                concede();
            }

            repaint();
        }
    }

    public int getX()
    {
        return currentX;
    }

    public int getY()
    {
        return currentY;
    }
    
    public boolean userQuit()
    {
        return quit;
    }
    
    public boolean userDone()
    {
        return userDone;
    }
    
    public void gameOver(GameState state)
    {
        String name = "";
        if (colour == Piece.BLUE) name = "Blue";
        else if (colour == Piece.RED) name = "Red";
        if (state == GameState.WON)
        {
            JOptionPane.showMessageDialog(null, name + " wins!", "Congratulations!", JOptionPane.DEFAULT_OPTION);
        }
        else if (state == GameState.LOST)
        {
            JOptionPane.showMessageDialog(null, name + " loses!", "Next time!", JOptionPane.DEFAULT_OPTION);
        }
        
        if (state != GameState.INCOMPLETE) dispose();
    }

    private class RenderPanel extends JPanel
    {
        private Polygon hex;
        private int hexWidth;

        public RenderPanel()
        {
            super();
            calculateHex();
        }
        
        private void calculateHex()
        {
            hex = new Polygon();
            
            hexWidth = 2 * (SWIDTH - 28) / ((board[0].length + 1) + 2 * (board.length));
            if ((board[0].length + 2) * (int)(hexWidth * Math.sqrt(3) / 2) > SWIDTH - 28) hexWidth = (SWIDTH - 28) / (board[0].length + 2);
            
            
            double side = hexWidth * Math.sqrt(3) / 3;
            hex.addPoint((int)(hexWidth/2), 0);
            hex.addPoint((int)(hexWidth), (int)(side/2));
            hex.addPoint((int)(hexWidth), (int)(3 * side / 2));
            hex.addPoint((int)(hexWidth/2), (int)(2 * side));
            hex.addPoint(0, (int)(3 * side / 2));
            hex.addPoint(0, (int)(side/2));
            
            hex.translate(14,14);
        }

        public void paintComponent(Graphics go)
        {
            super.paintComponent(go);
            Graphics2D g = (Graphics2D)go;
            
            RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g.setRenderingHints(rh);
            
            g.setColor(Color.WHITE);
            g.fillRect(0,0, SWIDTH, SWIDTH);
            g.setColor(Color.BLACK);
            
            Polygon brush = new Polygon(hex.xpoints, hex.ypoints, hex.npoints);
            brush.translate(hexWidth, 0);
            g.setColor(new Color(255, 100, 100));
            for (int i = 0; i < board.length - 1; i++)
            {
                g.fillPolygon(brush);
                brush.translate(hexWidth, 0);
            }
            
            brush = new Polygon(hex.xpoints, hex.ypoints, hex.npoints);
            brush.translate((board[0].length + 1) * hexWidth/2, (board[0].length + 1) * (int)(hexWidth * Math.sqrt(3) / 2));
            
            for (int i = 0; i < board.length - 1; i++)
            {
                g.fillPolygon(brush);
                brush.translate(hexWidth, 0);
            }
            
            brush = new Polygon(hex.xpoints, hex.ypoints, hex.npoints);
            brush.translate(0, (int)(hexWidth * Math.sqrt(3)));
            g.setColor(new Color(100, 100, 255));
            for (int j = 0; j < board[0].length - 1; j++)
            {
                g.fillPolygon(brush);
                brush.translate(hexWidth/2, (int)(hexWidth * Math.sqrt(3) / 2));
            }
            
            brush = new Polygon(hex.xpoints, hex.ypoints, hex.npoints);
            brush.translate((int)(hexWidth * (float)(board.length + 0.5)), (int)(hexWidth * Math.sqrt(3) / 2));
            
            for (int j = 0; j < board[0].length - 1; j++)
            {
                g.fillPolygon(brush);
                brush.translate(hexWidth/2, (int)(hexWidth * Math.sqrt(3) / 2));
            }
            
            g.setColor(Color.BLACK);
            brush = new Polygon(hex.xpoints, hex.ypoints, hex.npoints);
            brush.translate(hexWidth/2, (int)(hexWidth * Math.sqrt(3) / 2));
            for (int j = 0; j < board[0].length; j++)
            {
                for (int i = 0; i < board.length; i++)
                {
                    if (i == currentX && j == currentY)
                    {
                        g.setColor(Color.GREEN);
                        g.fillPolygon(brush);
                        g.setColor(Color.BLACK);
                    }

                    else if (board[i][j] == Piece.RED)
                    {
                        g.setColor(Color.RED);
                        g.fillPolygon(brush);
                        g.setColor(Color.BLACK);
                    }

                    else if (board[i][j] == Piece.BLUE)
                    {
                        g.setColor(Color.BLUE);
                        g.fillPolygon(brush);
                        g.setColor(Color.BLACK);
                    }

                    else g.drawPolygon(brush);
                    brush.translate(hexWidth, 0);
                }
                brush.translate(-1 * board.length * hexWidth + hexWidth/2, (int)(hexWidth * Math.sqrt(3) / 2));
            }
            
            String text = "";
            if (colour == Piece.BLUE) 
            {
                text = "You are the blue player";
                g.setColor(Color.BLUE);
            }
            else if (colour == Piece.RED)
            {
                text = "You are the red player";
                g.setColor(Color.RED);
            }
            
            FontMetrics metrics = g.getFontMetrics(g.getFont());
            int x = (SWIDTH - metrics.stringWidth(text)) / 2;
            g.drawString(text, x, SWIDTH - metrics.getHeight() - SWIDTH/7);
            text = "Use the arrow and enter keys to make your selection, and q to quit.";
            x = (SWIDTH - metrics.stringWidth(text)) / 2;
            g.setColor(Color.BLACK);
            g.drawString(text, x, SWIDTH - metrics.getHeight() - SWIDTH/8);
        }
    }
}