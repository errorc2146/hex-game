public class HumanPlayer implements PlayerInterface
{
    private Piece colour;
    UI_kdwm66 input;
    
    public HumanPlayer()
    {
        colour = Piece.UNSET;
        input = UI_kdwm66.getInstance();
    }
    
    public MoveInterface makeMove(Piece[][] boardView) throws NoValidMovesException
    {
        boolean positionAvailable = false;
        for (Piece[] column : boardView)
        {
            for (Piece piece : column)
            {
                if (piece == Piece.UNSET)
                {
                    positionAvailable = true;
                    break;
                }
                if (positionAvailable) break;
            }
        }
        if (!positionAvailable) throw new NoValidMovesException();
        
        Move move = new Move();
        
        input.setColour(colour);
        input.setBoard(boardView);
        
        while (!input.userDone()) //sorry
        {
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e){};
        }
        
        if (input.userQuit()) move.setConceded();
        try
        {
            move.setPosition(input.getX(), input.getY());
        }
        catch (InvalidPositionException e){}

        return move;
    }
    
    public boolean setColour(Piece colour) throws InvalidColourException, ColourAlreadySetException
    {
        if (this.colour != Piece.UNSET) throw new ColourAlreadySetException();
        else if (colour == Piece.UNSET) throw new InvalidColourException();
        this.colour = colour;
        return true;
    }
    
    public boolean finalGameState(GameState state)
    {
        input.setColour(colour);
        input.gameOver(state);
        return true;
    }
}
