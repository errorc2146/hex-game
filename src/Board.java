import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.AbstractMap;

public class Board implements BoardInterface
{
    private Piece board[][];
    private int sizeX, sizeY;
    private int blues, reds;
    
    public boolean setBoardSize(int sizeX, int sizeY)
            throws BoardAlreadySizedException, InvalidBoardSizeException
    {
        if (sizeX < 1 || sizeY < 1) throw new InvalidBoardSizeException();
        else if (this.sizeX != 0) throw new BoardAlreadySizedException();
        
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        board = new Piece[sizeX][sizeY];
        
        for (Piece[] column : board)
            Arrays.fill(column, Piece.UNSET);
        
        return true;
    }
    
    public Piece[][] getBoardView() throws NoBoardDefinedException
    {
        if (sizeX == 0) throw new NoBoardDefinedException();
        
        Piece[][] boardCopy = new Piece[sizeX][];
        
        for (int i = 0; i < sizeX; i++)
        {
            boardCopy[i] = Arrays.copyOf(board[i], board[i].length);
        }
        
        return boardCopy;
    }
    
    public boolean placePiece(Piece colour, MoveInterface move) 
            throws PositionAlreadyTakenException, InvalidPositionException, InvalidColourException, NoBoardDefinedException
    {
        if (board == null) throw new NoBoardDefinedException();
        
        int x = move.getXPosition();
        int y = move.getYPosition();
        
        if (x > sizeX - 1|| y > sizeY - 1) throw new InvalidPositionException();
        else if (board[x][y] != Piece.UNSET) throw new PositionAlreadyTakenException();
        
        if (colour == Piece.BLUE)
        {
            if (blues > reds) throw new InvalidColourException();
            blues++;
            board[x][y] = colour;
        }
        
        else if (colour == Piece.RED)
        {
            if (reds > blues) throw new InvalidColourException();
            reds++;
            board[x][y] = colour;
        }
        
        else if (colour == Piece.UNSET) throw new InvalidColourException();
        
        return true;
    }
    
    public Piece gameWon() throws NoBoardDefinedException
    {
        if (board == null) throw new NoBoardDefinedException();
        
        GameChecker checker = new GameChecker();
        if (checker.checkWon(Piece.BLUE)) return Piece.BLUE;
        if (checker.checkWon(Piece.RED)) return Piece.RED;
        return Piece.UNSET;
    }
    
    private class GameChecker
    {
        boolean[][] visited;
        
        private List<Map.Entry<Integer,Integer>> getAdjacentCoords(int x, int y)
        {
            List<Map.Entry<Integer,Integer>>  adjacent = new ArrayList<Map.Entry<Integer,Integer>>();
            for (int j = 0; j <=2; j++)
            {
                int y1 = y - 1 + j;
                if (y1 < 0) continue;
                else if (y1 >= sizeY) break;

                int x1 = x + j % 2;
                int x2 = x - 1;
                if (j == 0) x2 += 2;
                
                if (x1 >= 0 && x1 < sizeX) adjacent.add(new AbstractMap.SimpleEntry<>(x1,y1));
                if (x2 >= 0 && x2 < sizeX) adjacent.add(new AbstractMap.SimpleEntry<>(x2,y1));
            }
            return adjacent;
        }
        
        private boolean DFS(int x, int y, Piece colour)
        {
            //if we made it to the end of the board return true
            if (colour == Piece.BLUE && x == sizeX - 1) return true;
            else if (colour == Piece.RED && y == sizeY - 1) return true;

            //check if we're connected to any unvisited hexagons of the right colour
            //Call DFS on any we find, return true if it does
            
            for (Map.Entry<Integer,Integer> adj: getAdjacentCoords(x,y))
            {
                int adjx = adj.getKey();
                int adjy = adj.getValue();
                
                //if (x == 1 && y == 3) System.out.println(adjx + "," + adjy);
                
                if (board[adjx][adjy] == colour && !visited[adjx][adjy])
                {
                    visited[adjx][adjy] = true;
                    if (DFS(adjx, adjy, colour)) return true;
                }
            }
            
            //couldn't find any connected pieces that lead to the other end
            return false;
        }
        
        private boolean checkWon(Piece colour)
        {
            visited = new boolean[sizeX][sizeY];

            if (colour == Piece.BLUE)
            {
                //if (blues < sizeX) return false;
                for (int j = 0; j < sizeY; j++)
                {
                    if (board[0][j] == Piece.BLUE && !visited[0][j])
                    {
                        if (DFS(0,j, colour)) return true;
                    }
                }
            }

            else if (colour == Piece.RED)
            {
                if (reds < sizeY) return false;
                for (int i = 0; i < sizeX; i++)
                {
                    if (board[i][0] == Piece.RED && !visited[i][0])
                    {
                        if (DFS(i,0, colour)) return true;
                    }
                }
            }
            return false;
        }
    }
}