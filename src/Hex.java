public class Hex 
{
    public static void main(String[] args)
    {
        //test();
        GameManagerInterface wilk = new GameManager();
        PlayerInterface human1 = new HumanPlayer();
        PlayerInterface comp1 = new ComputerPlayer_kdwm66();
        try
        {
            wilk.specifyPlayer(human1, Piece.RED);
            wilk.specifyPlayer(comp1, Piece.BLUE);
            wilk.boardSize(14, 14);
        }
        catch (Exception e){}
        wilk.playGame();
    }
    
    public static void test()
    {
        BoardInterface board = new Board();
        
        try
        {
            board.setBoardSize(-1, 10);
        }
        catch (InvalidBoardSizeException e){System.out.println("Succeess!");}
        catch (Exception e){}
        
        try
        {
            board.setBoardSize(7, 7);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.setBoardSize(7, 7);
        }
        catch (BoardAlreadySizedException e){System.out.println("Success 2!");}
        catch (Exception e){System.out.println("Failure!");}
        
        board = new Board();
        
        try
        {
            board.getBoardView();
        }
        catch (NoBoardDefinedException e){System.out.println("Success 3!");}
        catch (Exception e){System.out.println("Failure!");}
        
        MoveInterface move = new Move();
        if (move.hasConceded()) System.out.println("Failure!");
        
        try
        {
            move.setPosition(-1,5);
        }
        catch (InvalidPositionException e){System.out.println("Success 4!");}
        
        try
        {
            move.setPosition(4,5);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        if (move.getXPosition() != 4 || move.getYPosition() != 5)
            System.out.println("Failure!");  
        
        try
        {
            board.placePiece(Piece.RED, move);
        }
        catch (NoBoardDefinedException e) {System.out.println("Success 5!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.setBoardSize(7, 7);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.placePiece(Piece.RED, move);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.placePiece(Piece.BLUE, move);
        }
        catch (PositionAlreadyTakenException e) {System.out.println("Success 6!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            move.setPosition(5,5);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.placePiece(Piece.BLUE, move);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            move.setPosition(1,1);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.placePiece(Piece.BLUE, move);
        }
        catch (InvalidColourException e){System.out.println("Success 7!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            move.setPosition(3,3);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            board.placePiece(Piece.UNSET, move);
        }
        catch (InvalidColourException e){System.out.println("Success 8!");}
        catch (Exception e){System.out.println("Failure!");}
        
        BoardInterface fullBoard = new Board();
        
        try
        {
            fullBoard.setBoardSize(7, 7);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        for (int i = 0; i <= 6; i++)
        {
            for (int j = 0; j <= 6; j++)
            {
                try
                {
                    move.setPosition(i,j);
                }
                catch (Exception e){System.out.println("Failure!");}
                
                try
                {
                    if ((i + j) % 2 == 0) fullBoard.placePiece(Piece.RED, move);
                    else fullBoard.placePiece(Piece.BLUE, move);
                }
                catch (PositionAlreadyTakenException e) {System.out.println(i + ", " + j);}
                catch (InvalidPositionException e) {System.out.println("Invalid pos!");}
                catch (InvalidColourException e) {System.out.println("Invalid col!");}
                catch (NoBoardDefinedException e) {System.out.println("What!");}
            }
        }
        
        PlayerInterface human = new HumanPlayer();
        PlayerInterface comp = new ComputerPlayer_kdwm66();
        
        try
        {
            human.setColour(Piece.UNSET);
        }
        catch (InvalidColourException e){System.out.println("Success 9!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            comp.setColour(Piece.UNSET);
        }
        catch (InvalidColourException e){System.out.println("Success 10!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            human.setColour(Piece.BLUE);
            comp.setColour(Piece.RED);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            human.setColour(Piece.BLUE);
        }
        catch (ColourAlreadySetException e){System.out.println("Success 11!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            comp.setColour(Piece.BLUE);
        }
        catch (ColourAlreadySetException e){System.out.println("Success 12!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            human.makeMove(fullBoard.getBoardView());
        }
        catch (Exception e){System.out.println("Success 13!");}
        
        try
        {
            comp.makeMove(fullBoard.getBoardView());
        }
        catch (Exception e){System.out.println("Success 14!");}
        
        GameManagerInterface manager = new GameManager();
        human = new HumanPlayer();
        comp = new ComputerPlayer_kdwm66();
        try
        {
            manager.specifyPlayer(human, Piece.UNSET);
        }
        catch (InvalidColourException e){System.out.println("Success 15!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.specifyPlayer(human, Piece.BLUE);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.specifyPlayer(comp, Piece.BLUE);
        }
        catch (ColourAlreadySetException e){System.out.println("Success 16!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.specifyPlayer(comp, Piece.RED);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.boardSize(-1, 7);
        }
        catch (InvalidBoardSizeException e){System.out.println("Success 17!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.boardSize(7, -7);
        }
        catch (InvalidBoardSizeException e){System.out.println("Success 18!");}
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.boardSize(7, 7);
        }
        catch (Exception e){System.out.println("Failure!");}
        
        try
        {
            manager.boardSize(8, 8);
        }
        catch (BoardAlreadySizedException e){System.out.println("Success 19!");}
        catch (Exception e){System.out.println("Failure!");}
        
        GameManagerInterface wilk = new GameManager();
        PlayerInterface human1 = new HumanPlayer();
        PlayerInterface comp1 = new ComputerPlayer_kdwm66();
        try
        {
            wilk.specifyPlayer(human1, Piece.RED);
            wilk.specifyPlayer(comp1, Piece.BLUE);
            wilk.boardSize(9, 9);
        }
        catch (Exception e){}
        System.out.println("wilk returned " + wilk.playGame());
    }
}
