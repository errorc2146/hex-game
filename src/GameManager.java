import javax.swing.JOptionPane;
import java.util.regex.Pattern;
import java.awt.Toolkit;

public class GameManager implements GameManagerInterface
{
    private PlayerInterface redPlayer;
    private PlayerInterface bluePlayer;
    private BoardInterface board;
    
    public GameManager()
    {
        board = new Board();
    }
    
    public boolean specifyPlayer(PlayerInterface player, Piece colour) throws ColourAlreadySetException, InvalidColourException
    { 
        if (colour == Piece.UNSET) throw new InvalidColourException();
        
        if (colour == Piece.RED)
        {
            if (redPlayer != null) throw new ColourAlreadySetException();
            if (!player.setColour(Piece.RED))
            {
                JOptionPane.showMessageDialog(null, "Couldn't specify red player!", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            redPlayer = player;
        }
        else if (colour == Piece.BLUE)
        {
            if (bluePlayer != null) throw new ColourAlreadySetException();
            if (!player.setColour(Piece.BLUE))
            {
                JOptionPane.showMessageDialog(null, "Couldn't specify blue player!", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }

            bluePlayer = player;
            
        }
        
        return true;
    }
    
    public boolean boardSize(int sizeX, int sizeY) throws InvalidBoardSizeException, BoardAlreadySizedException
    {
        return board.setBoardSize(sizeX, sizeY);
    }
    
    private boolean informPlayers(Piece winner)
    {
        if (winner == Piece.RED)
        {
            if(!bluePlayer.finalGameState(GameState.LOST) || !redPlayer.finalGameState(GameState.WON))
                return false;
        }
        else if (winner == Piece.BLUE)
        {
            if(!bluePlayer.finalGameState(GameState.WON) || !redPlayer.finalGameState(GameState.LOST))
                return false;
        }
        else return false;
        return true;
    }
    
    public boolean playGame()
    {
        if (redPlayer == null || bluePlayer == null)
        {
            JOptionPane.showMessageDialog(null, "Please pass me two PlayerInterfaces first!", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        boolean gameOver = false;
        while (!gameOver)
        {
            boolean validMove = false;
            while (!validMove && !gameOver) //Blue goes first
            {
                String error = "";
                try
                {
                    MoveInterface move = bluePlayer.makeMove(board.getBoardView());
                    
                    if (move.hasConceded())
                    {
                        gameOver = true;
                        if (!informPlayers(Piece.RED))
                            error = "Could not form a player of the final game state!";
                    }
                    
                    if (board.placePiece(Piece.BLUE, move))
                    {
                        validMove = true;
                        if (board.gameWon() == Piece.BLUE)
                        {
                            gameOver = true;
                            if (!informPlayers(Piece.BLUE))
                                error = "Could not form a player of the final game state!";
                        }
                    }
                    else error = "Could not place blues piece!";
                }
                catch (PositionAlreadyTakenException e)
                {
                    error = "Someone tried to place a piece in an already used spot!";
                }
                catch (InvalidPositionException e)
                {
                    error = "Someone tried to place a piece in a position that doesn't exist!";
                }
                catch (InvalidColourException e)
                {
                    error = "Someone tried to place an unset piece, or place two pieces in a row!";
                }
                catch (NoValidMovesException e)
                {
                   error = "Someone claims the board is full!";
                }
                catch (NoBoardDefinedException e)
                {
                    if (!boardPrompt()) return false;
                    else return playGame();
                }
                finally
                {
                    if (!error.equals(""))
                    {
                        JOptionPane.showMessageDialog(null, error, "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
                
            validMove = false;
            while (!validMove && !gameOver) //Then red
            {
                String error = "";
                try
                {
                    MoveInterface move = redPlayer.makeMove(board.getBoardView());
                    
                    if (move.hasConceded())
                    {
                        gameOver = true;
                        if (!informPlayers(Piece.BLUE))
                            error = "Could not form a player of the final game state!";
                    }
                    
                    if (board.placePiece(Piece.RED, move))
                    {
                        validMove = true;
                        if (board.gameWon() == Piece.RED)
                        {
                            gameOver = true;
                            if (!informPlayers(Piece.RED))
                                error = "Could not form a player of the final game state!";
                        }
                    }
                    else error = "Could not place reds piece!";
                }
                catch (PositionAlreadyTakenException e)
                {
                    error = "Someone tried to place a piece in an already used spot!";
                }
                catch (InvalidPositionException e)
                {
                    error = "Someone tried to place a piece in a position that doesn't exist!";
                }
                catch (InvalidColourException e)
                {
                    error = "Someone tried to place an unset piece, or place two pieces in a row!";
                }
                catch (NoValidMovesException e)
                {
                   error = "Someone claims the board is full!";
                }
                catch (NoBoardDefinedException e)
                {
                    if (!boardPrompt()) return false;
                    else return playGame();
                }
                finally
                {
                    if (!error.equals(""))
                    {
                        JOptionPane.showMessageDialog(null, error, "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
        return true;
    }
    
    private boolean boardPrompt()
    {
        String message = "There is no board currently defined, Would you like to create one?";
        int response = JOptionPane.showConfirmDialog(null, message, "No board defined", JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            String xIn = "";
            while (!Pattern.matches("[0-9]+", xIn))
            {
                xIn = JOptionPane.showInputDialog("x dimension:");
            }
            
            String yIn = "";
            while (!Pattern.matches("[0-9]+", yIn))
            {
                yIn = JOptionPane.showInputDialog("y dimension:");
            }
            
            try
            {
                boardSize(Integer.parseInt(xIn), Integer.parseInt(yIn));
            }
            catch (InvalidBoardSizeException e)
            {
                Toolkit.getDefaultToolkit().beep();
                return boardPrompt();
            }
            catch (BoardAlreadySizedException e){}
            
            return true;
        }
        
        return false;
    }
}
