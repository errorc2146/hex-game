import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Random;

public class ComputerPlayer_kdwm66 implements PlayerInterface
{
    private Piece colour;
    boolean piecePlaced;
    Random generator = new Random();
    
    class Coordinate
    {
        int x;
        int y;
        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        
        public boolean equals(Object o)
        {
            Coordinate other = (Coordinate)o;
            return x == other.x && y == other.y;
        }
        
        public int hashCode()
        {
            int temp = (y+((x+1)/2));
            return x +  (temp * temp);
        }
        
        public String toString()
        {
            return "(" + x + "," + y + ")";
        }
    }
            
    public ComputerPlayer_kdwm66()
    {
        piecePlaced = false;
        colour = Piece.UNSET;
        generator = new Random();
    }
    
    public MoveInterface makeMove(Piece[][] boardView) throws NoValidMovesException
    {
        boolean positionAvailable = false;
        int x = 0;
        int y = 0;
        Move move = new Move();
        
        for (x = 0; x < boardView.length && !positionAvailable; x++)
        {
            for (y = 0; y < boardView[0].length && !positionAvailable; y++)
            {
                if (boardView[x][y] == Piece.UNSET)
                {
                    positionAvailable = true;
                    if (!piecePlaced)
                    {
                        if (colour == Piece.BLUE)
                        {
                            x = boardView.length - 1;
                            y = boardView[0].length/4 + generator.nextInt(boardView[0].length/2);
                            
                            while (boardView[x][y] != Piece.UNSET)
                                y = generator.nextInt(boardView[0].length);
                        }
                        
                        else if (colour == Piece.RED)
                        {
                            y = boardView[0].length - 1;
                            x = generator.nextInt(boardView.length);

                            while (boardView[x][y] != Piece.UNSET)
                                x = generator.nextInt(boardView.length);
                        }
                        
                        try
                        {
                            move.setPosition(x,y);
                            piecePlaced = true;
                            return move;
                        }
                        catch (InvalidPositionException e){System.out.println("Ouch!");};
                    }
                }
            }
        }
        if (!positionAvailable) throw new NoValidMovesException();
        
        try
        {
            move.setPosition(x,y);
        }
        catch (InvalidPositionException e){}
        List<Coordinate> myShortestPath = getShortestPath(colour, boardView);
        List<Coordinate> theirShortestPath; 
        if (colour == Piece.BLUE) theirShortestPath = getShortestPath(Piece.RED, boardView);
        else theirShortestPath = getShortestPath(Piece.BLUE, boardView);
        
        Map <Coordinate, Integer> scores = new HashMap<Coordinate, Integer>();
        int pointsAwarded = 0;
        
        int myLength = 0;
        int theirLength = 0;
        
        for (Coordinate coord : myShortestPath)
        {
            if (boardView[coord.x][coord.y] == Piece.UNSET) myLength++;
        }
        for (Coordinate coord : theirShortestPath)
        {
            if (boardView[coord.x][coord.y] == Piece.UNSET) theirLength++;
        }
        
        if (myLength == 0 || theirLength == 0) return move;
        
        int aggression = myLength / theirLength;
        int increment = 1;
        int start = 0;
        List <Coordinate> path = myShortestPath;
        
        if (aggression >= 2) 
        {
            for (Coordinate coord: myShortestPath)
            {
                if (theirShortestPath.contains(coord))
                {
                    try
                    {
                        move.setPosition(coord.x, coord.y);
                    } catch (InvalidPositionException e){/*in this case we're screwed*/}
                    return move;
                }
            }
        }
        
        Coordinate firstCoord = path.get(0);
        if (boardView[firstCoord.x][firstCoord.y] == Piece.UNSET)
        {
            increment = -1;
            start = path.size() - 1;
        }
        for (int i = start; i >= 0 && i < path.size(); i += increment)
        {
            Coordinate next = path.get(i);
            if (boardView[next.x][next.y] == Piece.UNSET)
            {
                try
                {
                    move.setPosition(next.x,next.y);
                    break;
                }
                catch (InvalidPositionException e){}
            }
        }
        
        if (move == null) move.setConceded();
        return move;
    }
    
    private List<Coordinate> getAdjacentCoords(Coordinate in, int sizeX, int sizeY)
    {
        List<Coordinate>  adjacent = new ArrayList<Coordinate>();
        int x = in.x; int y = in.y;
        for (int j = 0; j <=2; j++)
        {
            int y1 = y - 1 + j;
            if (y1 < 0) continue;
            else if (y1 >= sizeY) break;

            int x1 = x + j % 2;
            int x2 = x - 1;
            if (j == 0) x2 += 2;

            if (x1 >= 0 && x1 < sizeX) adjacent.add(new Coordinate(x1,y1));
            if (x2 >= 0 && x2 < sizeX) adjacent.add(new Coordinate(x2,y1));
        }
        return adjacent;
    }

    private List<Coordinate> getShortestPath(Piece colour, Piece[][] boardState)
    {
        Map<Coordinate, Coordinate> predecessors = new HashMap<Coordinate, Coordinate>();
        Map<Coordinate, Integer> distances = new HashMap<Coordinate, Integer>();
        Queue<Coordinate> perimiter = new LinkedList<Coordinate>();
        
        int sizeX = boardState.length;
        int sizeY = boardState[0].length;
        
        //Basically Dijkstras from your end of the board, the actual side doesn't matter
        if (colour == Piece.BLUE)
        {
            for (int j = 0; j < sizeY; j++)
            {
                Coordinate coord = new Coordinate(-1,j);
                perimiter.add(coord);
                predecessors.put(coord, null);
                distances.put(coord, 0);
            }
        }
        
        else
        {
            for (int i = 0; i < sizeX; i++)
            {
                Coordinate coord = new Coordinate(i,-1);
                perimiter.add(coord);
                predecessors.put(coord, null);
                distances.put(coord, 0);
            }
        }
        
        while (perimiter.size() != 0)
        {
            Coordinate current = perimiter.poll();
            for (Coordinate neighbor : getAdjacentCoords(current, sizeX, sizeY))
            {
                if (boardState[neighbor.x][neighbor.y] == Piece.UNSET || boardState[neighbor.x][neighbor.y] == colour)
                {
                    boolean closerThanPred = false;
                    int myDistance = distances.get(current);
                    if (boardState[neighbor.x][neighbor.y] == Piece.UNSET) myDistance += 1;
                    
                    if (!predecessors.containsKey(neighbor)) 
                    {
                        closerThanPred = true;
                        perimiter.add(neighbor);
                    }
                    
                    else if (myDistance < distances.get(neighbor)) closerThanPred = true;
                    
                    if (closerThanPred)
                    {
                        predecessors.put(neighbor, current);
                        distances.put(neighbor, myDistance);
                    }
                }
            }
        }
        
        //check the distances at the other side of the board
        Coordinate best = null;
        int bestDistance = sizeX * sizeY + 1;
        
        if (colour == Piece.BLUE)
        {
            for (int j = 0; j < sizeY; j++)
            {
                Coordinate coord = new Coordinate(sizeX-1, j);
                if (distances.containsKey(coord))
                {
                    if (distances.get(coord) < bestDistance)
                    {
                        best = coord;
                        bestDistance = distances.get(coord);
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < sizeX; i++)
            {
                Coordinate coord = new Coordinate(i, sizeY - 1);
                if (distances.containsKey(coord))
                {
                    if (distances.get(coord) < bestDistance)
                    {
                        best = coord;
                        bestDistance = distances.get(coord);
                    }
                }
            }
        }
        
        //construct the shortest path and return it
        List<Coordinate> shortestPath = new ArrayList<Coordinate>();
        if (best != null)
        {
            Coordinate iterator = best;
            while (iterator.x != -1 && iterator.y != -1)
            {
                shortestPath.add(iterator);
                iterator = predecessors.get(iterator);
            }
        }
        
        return shortestPath;
    }
    
    public boolean setColour(Piece colour) throws InvalidColourException, ColourAlreadySetException
    {
        if (this.colour != Piece.UNSET) throw new ColourAlreadySetException();
        else if (colour == Piece.UNSET) throw new InvalidColourException();
        this.colour = colour;
        return true;
    }
    
    public boolean finalGameState(GameState state)
    {
        return true;
    }
}
