import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import javax.swing.JOptionPane;

public class GameManager2 implements GameManagerInterface
{
    Map<Piece, PlayerInterface> players;
    BoardInterface board;
    
    public GameManager2()
    {
        board = new Board();
        players = new LinkedHashMap<Piece, PlayerInterface>();
    }
    
    public boolean specifyPlayer(PlayerInterface player, Piece colour) throws ColourAlreadySetException, InvalidColourException
    {
        if (colour != Piece.BLUE || colour != Piece.RED) throw new InvalidColourException();
        if (players.size() == 2 || players.containsKey(colour)) throw new ColourAlreadySetException();
        
        players.put(colour, player);
        return true;
    }
    
    private boolean informPlayers(Piece winner)
    {
        if (players.size() != 2)
        {
            JOptionPane.showMessageDialog(null, "Please pass me two PlayerInterfaces first!", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        Piece loser;
        if (winner == Piece.RED) loser = Piece.BLUE;
        else loser = Piece.RED;
        
        if (!players.get(winner).finalGameState(GameState.WON) || !players.get(loser).finalGameState(GameState.LOST)) 
        
        return true;
    }
    
    public boolean boardSize(int sizeX, int sizeY) throws InvalidBoardSizeException, BoardAlreadySizedException
    {
        return board.setBoardSize(sizeX, sizeY);
    }
    
    private enum myPiece
    {
        RED(Piece.RED), BLUE(Piece.BLUE);
        
        private final Piece coPiece;
        
        myPiece(Piece coPiece)
        {
            this.coPiece = coPiece;
        }
        
        public Piece toPiece()
        {
            return coPiece;
        }
    }
    
    public boolean playGame()
    {       
        if (players.size() != 2)
        {
            JOptionPane.showMessageDialog(null, "Please pass me two PlayerInterfaces first!", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        Random generator = new Random();
        int turn = generator.nextInt(2);
        boolean gameOver = false;
        
        while (!gameOver)
        {
            Piece currentColour = myPiece.values()[turn].toPiece();
            PlayerInterface currentPlayer = players.get(currentColour);
            boolean validMove = false;
            
            while (!validMove)
            {
                String error = "";
                
                try
                {
                    MoveInterface move = currentPlayer.makeMove(board.getBoardView());

                    if (move.hasConceded())
                    {
                        turn = 1 - turn;
                        gameOver = true;
                    }

                    else
                    {
                        board.placePiece(currentColour, move);

                        if (board.gameWon() == currentColour)
                        {
                            gameOver = true;
                        }
                        else turn = 1 - turn;
                    }
                    validMove = true;
                }
                catch (PositionAlreadyTakenException e)
                {
                    error = "Someone tried to place a piece in an already used spot!";
                }
                catch (InvalidPositionException e)
                {
                    error = "Someone tried to place a piece in a position that doesn't exist!";
                }
                catch (InvalidColourException e)
                {
                    error = "Someone tried to place an unset piece, or place two pieces in a row!";
                }
                catch (NoValidMovesException e)
                {
                   error = "Someone claims the board is full!";
                }
                catch (NoBoardDefinedException e)
                {
                    if (!boardPrompt()) return false;
                    else return playGame();
                }
                finally
                {
                    if (!error.equals(""))
                    {
                        JOptionPane.showMessageDialog(null, error, "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }   
        }
        
        if (!informPlayers(myPiece.values()[turn].toPiece())) return false;
        return true;
    }
        
    
    private boolean boardPrompt()
    {
        String message = "There is no board currently defined, Would you like to create one?";
        int response = JOptionPane.showConfirmDialog(null, message, "No board defined", JOptionPane.YES_NO_OPTION);
        boolean finished = false;
        
        while (!finished)
        {
            int x = 0;
            int y = 0;

            boolean validInput = false;
            while (!validInput)
            {
                validInput = true;
                String xIn = JOptionPane.showInputDialog("x dimension:");

                try
                {
                    x = Integer.parseInt(xIn);
                }
                catch (NumberFormatException e){validInput = false;}
            }

            validInput = false;
            while (!validInput)
            {
                validInput = true;
                String yIn = JOptionPane.showInputDialog("y dimension:");

                try
                {
                    y = Integer.parseInt(yIn);
                }
                catch (NumberFormatException e){validInput = false;}
            }

            try
            {
                boardSize(x, y);
                finished = true;
            }
            catch (Exception e){};
        }
        
        return true;
    }
        
}
